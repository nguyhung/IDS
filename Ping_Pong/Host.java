import java.io.IOException;

import com.rabbitmq.client.*;
import com.rabbitmq.client.ConnectionFactory;

public class Host{
    private static final String EXCHANGE_NAME = "topic_logs";
    private static final String QUEUE_NAME = "message_queue";

    public static void main(String[] argv) throws Exception {
        int ID= (int)Math.random();
        

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");

        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();


        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, "start");

        Client cl= new Client(factory, connection, QUEUE_NAME, channel, EXCHANGE_NAME, ID);

        cl.out("start");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            // Receive start
            String message = new String(delivery.getBody(), "UTF-8");
            if (message.equals("start") && cl.clientState.equals("idle")) {
                cl.clientState = "waiting";
                cl.initConn(ID);
            }
            
            
            else if (message.equals("ping")){
                cl.pong();
            }
            else if (message.equals("pong")){
                cl.ping();
            }
            else{
                cl.in(message);
            }
        };

        // cl.req();

        channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> { });
    }
}
  