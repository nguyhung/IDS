import java.io.IOException;
import com.rabbitmq.client.*;
import com.rabbitmq.client.ConnectionFactory;

import java.lang.Math;


interface comm_itf{
  public void in(String message) ;
  public void out(String message) throws IOException ;
}
// interface client_itf {
//   public void req() ;
  
// }
interface message_itf  {
  public void initConn(int id) ;
  public void okConn() ;
  public void ping() ;
  public void pong () ; 
  
}

class Client implements  comm_itf,message_itf{
  private static String EXCHANGE_NAME;
  private static String QUEUE_NAME;
  
  private static  ConnectionFactory factory;
  private static  Connection connection;
  private static  Channel channel;
//   private static  String queueName;
  private static  int id;
  public String clientState = "idle";
  public boolean canSendPing = true;

  public Client (ConnectionFactory Factory, Connection Connection, String queueName, Channel Channel, String exchangeName, int ID) 
  {     
        factory = Factory;
        connection = Connection;
        channel = Channel;
        EXCHANGE_NAME = exchangeName;
        QUEUE_NAME = queueName;
        id = ID;
  }


    public void in(String message) {
        

            String[] parts = message.split(":");
                if (parts.length == 2 && parts[0].equals("init_conn")) {
                    System.out.println(" [x] Received " + message + " ");

                    int senderId = Integer.parseInt(parts[1]);
                    if (senderId > id) {
                        clientState = "started";
                            okConn();
                    } else if (senderId <= id && clientState.equals("idle")) {
                            initConn(id);
                            clientState =  "waiting";
                    
                    }else if (message.equals("ok_conn")) {
                        clientState = "started";
                        //channel.basicPublish(EXCHANGE_NAME,"", null, id);
                        ping();
                    }

                }
        
    }
        

    public void out(String message) throws IOException{
        channel.basicPublish(EXCHANGE_NAME, QUEUE_NAME, null, message.getBytes("UTF-8"));
        System.out.println("Sent: " + message);
    }
  
    public void ping()  {
        try{
            if (clientState.equals("started") && canSendPing) {
            out("ping");
            canSendPing = false;
            }
        }catch(IOException e){

        }
    }

    
    public void pong() {
        try{
            out("pong");
        }
        catch(IOException e){

        }
    }

    public void initConn(int id)  {
        try{
            out("init_conn:" + id);
        }
        catch(IOException e){

        }
    }

    
    public void okConn() {
        try{
            out("ok_conn");
        }catch(IOException e){

        }
    }

    
}
  

