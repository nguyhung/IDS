import java.rmi.*;
import java.util.List;

public interface chat_itf extends Remote{
    public String getName() throws RemoteException;
    
    public void send(String s) throws RemoteException;
    
    public void setClient( chat_itf c) throws RemoteException;
    
    public chat_itf getClient() throws RemoteException;

    public void broadcast(String s) throws RemoteException;

    public void addClient(chat_itf c) throws RemoteException;

    public void removeClient(chat_itf c) throws RemoteException;

    List<String> getChatHistory() throws RemoteException;
}
