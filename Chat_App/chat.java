

import java.io.*;
import java.rmi.*;
import java.rmi.server.*;
import java.util.ArrayList;
import java.util.List;


public class chat extends UnicastRemoteObject implements chat_itf {
 
    public String name;
    public chat_itf client = null;
    private List<chat_itf> clients = new ArrayList<>();
    private List<String> chatHistory = new ArrayList<>();

    private static final String CHAT_HISTORY_FILE = "chat_history.txt";
 
    public chat(String n) throws RemoteException { 
        this.name = n;  
        loadChatHistory(); // Load chat history when the server starts up 
    }
    public String getName() throws RemoteException {
        return this.name;
    }
 
    public void setClient(chat_itf c){
        client = c;
    }
 
    public chat_itf getClient(){
        return client;
    }
 

    public void send(String s) throws RemoteException{
        System.out.println(s);
        chatHistory.add(s); // Add message to chat history
        saveChatHistory(); // Save chat history to file

    }

    
    public void broadcast(String s) throws RemoteException {
        System.out.println("[Broadcast] " + s);
        for (chat_itf c : clients) {
            if (c != client) {
                c.send(s);
            }
        }
    }

    public void addClient(chat_itf c) throws RemoteException {
        clients.add(c);
    }
    
    public void removeClient(chat_itf c) throws RemoteException {
        clients.remove(c);
    }

    public List<String> getChatHistory() throws RemoteException {
        return chatHistory;
    }

    private void loadChatHistory() {
        try (BufferedReader reader = new BufferedReader(new FileReader(CHAT_HISTORY_FILE))) {
            String line;
            while ((line = reader.readLine()) != null) {
                chatHistory.add(line);
            }
        }catch (FileNotFoundException e) {
            System.out.println("Chat history file not found. Creating new file.");
            // If the file doesn't exist, create a new one
            createChatHistoryFile();
        } catch (IOException e) {
            System.err.println("Error loading chat history: " + e.getMessage());
        }
    }

    private void createChatHistoryFile() {
        try {
            File file = new File(CHAT_HISTORY_FILE);
            if (file.createNewFile()) {
                System.out.println("Chat history file created successfully.");
            } else {
                System.out.println("Chat history file already exists.");
            }
        } catch (IOException e) {
            System.err.println("Error creating chat history file: " + e.getMessage());
        }
    }

    public void saveChatHistory() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(CHAT_HISTORY_FILE,false))) {
            for (String message : chatHistory) {
                writer.write(message);
                writer.newLine();
            }
        } catch (IOException e) {
            System.err.println("Error saving chat history: " + e.getMessage());
        }
    }


}