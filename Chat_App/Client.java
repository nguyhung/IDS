import java.rmi.*;
import java.rmi.server.*;
import java.util.*;
import  java.rmi.registry.*;

public class Client   {


    public static void main(String args[]) {

        try {

            Scanner s = new Scanner(System.in);
            System.out.println("Enter your nickname to start chatting");
            String name = s.nextLine().trim();
            chat_itf client = new chat(name);

            chat_itf server = (chat_itf)Naming.lookup("CHAT");
            String msg = "["+client.getName()+"] s'est connecte";
            server.broadcast(msg);
            server.addClient(client);

            System.out.println("[System] You're ready to start chatting ! To quit, type: </quit>):");

            server.setClient(client);

            // Display chat history
            List<String> chatHistory = server.getChatHistory();
            System.out.println("[System] Chat History:");
            for (String message : chatHistory) {
                System.out.println(message);
            }


            while(true){
                msg = s.nextLine().trim();

                if (msg.equalsIgnoreCase("/quit")) { 
                    server.removeClient(client); 
                    // System.out.println("[System] Vous avez quitte le CHAT");
                    server.broadcast(name +  " has left the chat.");
                    break;
                }
                
                msg = client.getName()+" : "+msg;
                server.send(msg);
                server.broadcast(msg);
            }

            System.exit(0);


        }catch (Exception e) {
            e.printStackTrace();
        }


    }


}
