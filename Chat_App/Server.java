import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.*;
import java.rmi.server.*;
import java.util.*;
 import  java.rmi.registry.*;
public class Server  {
    
public static void main (String[] argv) {
    try {

            Scanner s = new Scanner(System.in);
            System.out.println("Enter the group chat'sname");
            String name=s.nextLine().trim();
            
 
            chat server = new chat(name);
            Registry registre = LocateRegistry.createRegistry(1099);
            Naming.rebind( "CHAT",server);

            

            System.out.println(name + " is ready:");

            while(true){
                String msg = s.nextLine().trim();
                if (msg.equalsIgnoreCase("/quit")) { 
                    System.out.println("Server shutting down");
                    break;
                }

                if (server.getClient() != null){
                    chat_itf Client = server.getClient();
                    msg = server.getName()+" : "+msg;
                    Client.send(msg);
                    server.broadcast(msg);
                                  
                }
                
            }

            System.exit(0);
 
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}