The current chat application allows multiple clients to exchange message via a shared server.
The architecture is simple, consists of client programs, server program and a remote interface that allows clients to send and receive the messages 
from each other as well as server.

Currently, the program and all of its registries are always running on port 1099 by default

Usage:
- simply compile the file with javac *.java
- start the server first with : java Server
- start the clients with:       java Client
- Once inside the chat application:
